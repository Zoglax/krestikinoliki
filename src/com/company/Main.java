package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        int n=3, m=3;
        int k=0;
        char[][] closeField = new char[n][m];

        final char STAR_SYMBOL = '*';
        final char SIGN_OF_FIRST_PLAYER = 'X';
        final char SIGN_OF_SECOND_PLAYER = 'O';
        char victory='N';

        int i1,j1,i2,j2;

        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                closeField[i][j] = STAR_SYMBOL;
            }
        }

        for (int i=0;i<50;i++)
        {
            System.out.println();
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(closeField[i][j] + " ");
            }
            System.out.println();
        }

        do
        {
            for (int i=0;i<50;i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            do
            {

                System.out.print("input i1: ");
                i1 = scanner.nextInt();

                System.out.print("input j1: ");
                j1 = scanner.nextInt();
            }
            while (closeField[i1][j1]!=STAR_SYMBOL);

            closeField[i1][j1]=SIGN_OF_FIRST_PLAYER;

            for (int i=0;i<50;i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            //проверка выигрыша 1-го игрока

            if((closeField[0][0]=='X' && closeField[1][1] =='X' && closeField[2][2] == 'X')||
                    (closeField[0][2]=='X' && closeField[1][1] =='X' && closeField[2][0] == 'X'))
            {
                victory='X';
            }
            else
            {
                for (int i=0; i<n; i++)
                {
                    if ((closeField[i][0]=='X' && closeField[i][1] =='X' && closeField[i][2] == 'X')||
                            (closeField[0][i]=='X' && closeField[1][i] =='X' && closeField[2][i] == 'X'))
                    {
                        victory='X';
                    }
                }
            }
            if (victory=='X')
            {
                break;
            }

            if(k==4)
            {
                break;
            }

            do {
                System.out.print("input i2: ");
                i2 = scanner.nextInt();

                System.out.print("input j2: ");
                j2 = scanner.nextInt();
            }
            while (closeField[i2][j2]!=STAR_SYMBOL);

            closeField[i2][j2]=SIGN_OF_SECOND_PLAYER;

            for (int i=0;i<50;i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            //проверка выигрыша 2-го игрока

            if((closeField[0][0]=='O' && closeField[1][1] =='O' && closeField[2][2] == 'O')||
                    (closeField[0][2]=='O' && closeField[1][1] =='O' && closeField[2][0] == 'O'))
            {
                victory='O';
            }
            else
            {
                for (int i=0; i<n; i++)
                {
                    if ((closeField[i][0]=='O' && closeField[i][1] =='O' && closeField[i][2] == 'O')||
                            (closeField[0][i]=='O' && closeField[1][i] =='O' && closeField[2][i] == 'O'))
                    {
                        victory='O';
                    }
                }
            }
            if (victory=='O')
            {
                break;
            }
            k++;
        }
        while (true);

        for (int i=0;i<50;i++)
        {
            System.out.println();
        }

        if (victory=='X')
        {
            System.out.println("1ST PLAYER WINS!");
        }
        else if (victory=='O')
        {
            System.out.println("2ND PLAYER WINS!");
        }
        else
        {
            System.out.println("NO ONE WON!");
        }


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(closeField[i][j] + " ");
            }
            System.out.println();
        }
    }

}
